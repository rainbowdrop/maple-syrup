#pragma once
#include <vector>
#include <string>

class StreamReader
{
	std::vector<char> packet_buffer;
	int read_offset = 0;

	char * AtCurrentPos( );

public:
	StreamReader( char * buf, int len );

	int GetCurrentOffset( );
	void SetCurrentOffset( int offset );

	bool HasData( int desired_size = 1 );

	long long ReadLongLong( );
	int ReadInt( );
	std::vector<char> GetVector( );
	char * GetBuffer( );
	int GetBufferLength( );
	int GetRemainingLength( );

	// if length is 0, read til null terminator
	std::string ReadString( int len = 0 );
	short ReadSignedShort( );
	unsigned short ReadShort( );
	float ReadFloat( );
	unsigned char ReadByte( );
	// any discards are pretty much unknown values.
	void DiscardBytes( int count );

	template <typename t>
	bool ReadType( t& out_val )
	{
		if ( !HasData( sizeof( t ) ) )
			return false;

		out_val = *( t * )this->AtCurrentPos( );
		read_offset += sizeof( t );

		return true;
	}
};
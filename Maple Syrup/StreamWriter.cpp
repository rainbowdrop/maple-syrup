#include "StreamWriter.hpp"
#include "Utility.hpp"

char * StreamWriter::AtCurrentPos( )
{
	return this->packet_buffer.data( ) + this->write_offset;
}

int StreamWriter::GetCurrentOffset( )
{
	return this->write_offset;
}

void StreamWriter::SetCurrentOffset( int offset )
{
	this->write_offset = offset;
}

int StreamWriter::GetLength( )
{
	return packet_buffer.size( );
}

void StreamWriter::IncreaseBuffer( int amount )
{
	auto necessary_size = ( write_offset + amount ) - packet_buffer.size( );
	this->packet_buffer.resize( this->packet_buffer.size( ) + necessary_size );
}

bool StreamWriter::HasSpace( int type_size )
{
	if ( !type_size )
		type_size = 1;

	return !( write_offset + type_size > packet_buffer.size( ) );
}

StreamWriter::StreamWriter( )
{
}

StreamWriter::StreamWriter( char * buf, int len )
{
	this->packet_buffer.assign( buf, buf + len );
}

void StreamWriter::WriteInt( int towrite )
{
	if ( !HasSpace( sizeof( towrite ) ) )
		this->IncreaseBuffer( sizeof( towrite ) );

	*( decltype( towrite ) * )this->AtCurrentPos( ) = towrite;
	write_offset += sizeof( towrite );
}

void StreamWriter::WriteString( const char * towrite, int len )
{
	if ( len == 0 )
		len = strlen( towrite );

	if ( !HasSpace( len ) )
		this->IncreaseBuffer( len );

	memcpy( this->AtCurrentPos( ), towrite, len );
	write_offset += len;
}

void StreamWriter::WriteShort( unsigned short towrite )
{
	if ( !HasSpace( sizeof( towrite ) ) )
		this->IncreaseBuffer( sizeof( towrite ) );

	*( decltype( towrite ) * )this->AtCurrentPos( ) = towrite;
	write_offset += sizeof( towrite );
}

void StreamWriter::WriteSignedShort( short towrite )
{
	if ( !HasSpace( sizeof( towrite ) ) )
		this->IncreaseBuffer( sizeof( towrite ) );

	*( decltype( towrite ) * )this->AtCurrentPos( ) = towrite;
	write_offset += sizeof( towrite );
}

void StreamWriter::WriteFloat( float towrite )
{
	if ( !HasSpace( sizeof( towrite ) ) )
		this->IncreaseBuffer( sizeof( towrite ) );

	*( decltype( towrite ) * )this->AtCurrentPos( ) = towrite;
	write_offset += sizeof( towrite );
}

void StreamWriter::WriteByte( unsigned char towrite )
{
	if ( !HasSpace( sizeof( towrite ) ) )
		this->IncreaseBuffer( sizeof( towrite ) );

	*( decltype( towrite ) * )this->AtCurrentPos( ) = towrite;
	write_offset += sizeof( towrite );
}

void StreamWriter::SkipBytes( int count )
{
	if ( !HasSpace( count ) )
		this->IncreaseBuffer( count );

	this->write_offset += count;
}

void StreamWriter::Trim( )
{
	this->packet_buffer.resize( this->write_offset );
}

std::vector<char> StreamWriter::GetBuffer( )
{
	return this->packet_buffer;
}

void StreamWriter::WriteLongLong( long long towrite )
{
	if ( !HasSpace( sizeof( towrite ) ) )
		this->IncreaseBuffer( sizeof( towrite ) );

	*( decltype( towrite ) * )this->AtCurrentPos( ) = towrite;
	write_offset += sizeof( towrite );
}
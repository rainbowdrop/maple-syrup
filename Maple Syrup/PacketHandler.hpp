#pragma once
#include "MapleStructs.hpp"
#include <map>
#include <vector>

#undef SendMessage
namespace PacketHandler
{
	// whether we modify damage packets to send 15 hits .
	extern bool instagib_enabled, knockback_hack;


	void DumpPacket( char * buf, int len, const char * name );
	void DumpPacket( char * buf, int len, int pid );

	std::vector<char> HandlePacket( char * unenc_buffer, int len, bool send );

	using handler = std::vector<char>( * )( char * unenc_buffer, int len );
	std::vector<char> UnknownPacketHandler( char * buf, int len );
	std::vector<char> UnknownPacketHandler( char * buf, int len, bool send );
#define QH(x) std::vector<char> x(char* buf, int len)


#pragma region Send Handlers

	std::vector<char> SendMessage( char * buf, int len );
	std::vector<char> SendMove( char * buf, int len );
	std::vector<char> SendMonsterPos( char * buf, int len );
	std::vector<char> SelectWorld( char * buf, int len );
	std::vector<char> SelectChannel( char * buf, int len );
	std::vector<char> SendPIN( char * buf, int len );
	std::vector<char> SelectCharacter( char * buf, int len );
	std::vector<char> SendAttack( char * buf, int len );
	std::vector<char> SendTakeDamage( char * buf, int len );
	std::vector<char> SendItemPickup( char * buf, int len );
	std::vector<char> SendSpendSkillPoint( char * buf, int len );
	std::vector<char> CharLoad_Hyper( char * buf, int len );
	std::vector<char> GPUStuff( char * buf, int len );
#pragma endregion

#pragma region Recv Handlers
	std::vector<char> ReceiveServers( char * buf, int len );
	std::vector<char> ReceiveMonsterHP( char * buf, int len );
	std::vector<char> ReceiveMapBounds( char * buf, int len );
	std::vector<char> ReceiveChatMessage( char * buf, int len );
	std::vector<char> ReceiveItemDropNotification( char * buf, int len );
	std::vector<char> ReceiveMessage( char * buf, int len );
	std::vector<char> ReceiveChannels( char * buf, int len );
	std::vector<char> ListCharacters( char * buf, int len );
	std::vector<char> ListEvents( char * buf, int len );
	std::vector<char> CharacterLogin( char * buf, int len );
	std::vector<char> SpawnCharacter( char * buf, int len );
	std::vector<char> SpawnMonster( char * buf, int len );
	std::vector<char> UpdateStat( char * buf, int len ); // works on heals, decrements, etc.
	std::vector<char> SomeHWIDStuff( char * buf, int len );
	std::vector<char> SelectCharacterResponse( char * buf, int len );
	std::vector<char> MoveMonsterResponse( char * buf, int len );
	std::vector<char> InvalidPINResponse( char * buf, int len );
	std::vector<char> UpdateEncryptedPacketIDs( char * buf, int len );

#pragma endregion

#pragma region Heartbeats
	std::vector<char> HeartBeat_6Bytes( char * buf, int len );
	std::vector<char> HeartBeat_8Bytes( char * buf, int len );
	std::vector<char> HeartBeat_2Bytes( char * buf, int len );
	std::vector<char> HeartBeat_3Bytes( char * buf, int len );
#pragma endregion

#define HANDLER(id, fn) {id, fn},
#define HANDLER_TODO(id) {id, UnknownPacketHandler},

	// Used when we get an encrypted_pids packet
	extern std::map<short, handler> encrypted_handlers;

	const std::map<short, handler> packet_handlers =
	{
		/*SEND HEARTBEATS*/
		HANDLER_TODO( 0x67, HeartBeat_8Bytes )
		HANDLER_TODO( 0xA2, HeartBeat_6Bytes )
		HANDLER( 0x9B, HeartBeat_6Bytes ) // i have seen a 009B packet with size 0x22.
		HANDLER( 0x12, HeartBeat_2Bytes )
		HANDLER( 0x88, HeartBeat_6Bytes )
		HANDLER_TODO( 0x68, HeartBeat_3Bytes )
		HANDLER_TODO( 0xA0, HeartBeat_3Bytes )
		HANDLER_TODO( 0x74, HeartBeat_2Bytes )
		HANDLER_TODO( 0xC6, HeartBeat_2Bytes )
		HANDLER_TODO( 0x49E, HeartBeat_2Bytes )
		HANDLER_TODO( ( short )0xB5A4, HeartBeat_8Bytes )
		/*SEND HEARTBEATS*/

		/*SEND HANDLERS*/
		//HANDLER( 0x66, SendMove ) // might actually be 0x1BBC? or 0x501?
		HANDLER( 0x6A, SelectWorld )
		HANDLER( 0x6B, SelectChannel )
		HANDLER( 0x6C, SendPIN ) // NOTE TO SELF: A LOT OF THESE ONE BYTERS ARE WRONG IDS FROM WHEN I WAS BAD.
		HANDLER( 0x6D, SelectCharacter )
		//HANDLER( 0x11BD, SendSpell ) // or is this a melee attack, magic attack, or ranged attack? idk.
		//HANDLER( 0x1588, CharLoad_Hyper )
		HANDLER_TODO( 0xC2 ) // like a HELLO packet, asking server if our connection is still alive.
		HANDLER_TODO( 0xDA ) // Hyper but with an extra 5 bytes.
		HANDLER_TODO( 0x70 ) // related to logging in, of course.
		HANDLER_TODO( 0xA1C ) // everything else is 0...?
		HANDLER_TODO( 0xAD5 ) // also.
		HANDLER_TODO( 0x112 )
		HANDLER_TODO( 0x1112 ) // also.
		HANDLER_TODO( 0x12E5 )
		HANDLER_TODO( 0x81D )
		HANDLER_TODO( 0xCC6 )
		HANDLER_TODO( ( short )0xB8A0 ) // Spells/skills // for some reason this needs a special cast...
		HANDLER( 0xFFFF, GPUStuff )
		/*RECV HANDLERS*/
		HANDLER( 0x1, ReceiveServers )
		HANDLER( 0x6, ReceiveChannels ) // idunno, its kinda useless.
		HANDLER( 0x7, ListCharacters )
		HANDLER( 0x8, SelectCharacterResponse ) // Probably an acceptance of our character selection
		HANDLER( 0x2E, UpdateEncryptedPacketIDs ) // modifies things like SendMessage packet id and MORE // probably recv world info.
		HANDLER( 0x4B, InvalidPINResponse )
		HANDLER( 0x62, UpdateStat ) // maybe?
		HANDLER( 0xA1, ReceiveMessage ) // or ReceiveEvent, idk.
		HANDLER( 0x12D, SomeHWIDStuff )
		HANDLER( 0x132, ListEvents ) // IDK. looks like 
		HANDLER( 0x21B, CharacterLogin )
		HANDLER( 0x295, SpawnCharacter ) // maybe?
		HANDLER( 0x297, ReceiveChatMessage )
		HANDLER( 0x493, SpawnMonster ) // 0x495 appears to be exact same packet, sometimes however is only 7 bytes with the rest cut off, dunno why.
		HANDLER( 0x495, SpawnMonster ) // literally exact same packet with 1 byte changed. Whether we control the monster or not?
		HANDLER( 0x49B, MoveMonsterResponse )
		HANDLER( 0x4A8, ReceiveMonsterHP )
		HANDLER( 0x501, ReceiveMapBounds ) // no fucking clue
		HANDLER( 0x51F, ReceiveItemDropNotification )
		HANDLER_TODO( 0x7B3 ) // mission stuff.
		HANDLER_TODO( 0x7B2 ) // mission stuff.
		HANDLER_TODO( 0xC9 ) // receive notification (Earned forever single title!) for example
		HANDLER_TODO( 0x73 ) // weird variable length strings, dont seem super related.
		/*RECV HEARTBEATS*/
		HANDLER_TODO( 0x12, HeartBeat_RECV_2Bytes )
		HANDLER_TODO( 0x19, Heartbeat_RECV_6Bytes )
		HANDLER_TODO( 0x9 ) // Heartbeat? idk contains some kinda serial/checksumidk.

	};
#undef HANDLER
}